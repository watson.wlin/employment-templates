#### Professional Services Engineer

<details>
<summary>New Team Member</summary>

1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls
1. [ ] Join the ['#eng-week-in-review' channel](https://app.slack.com/client/T02592416/CJWA4E9UG)

**Advanced Technical Topics**
1. [ ] Backup omnibus using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
1. [ ] Install GitLab via [Docker](https://docs.gitlab.com/ce/install/docker.html)
1. [ ] Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)
1. [ ] Starting a [rails console](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#invoking-rake-tasks)
1. [ ] Learn about environment Information and [maintenance checks](http://docs.gitlab.com/ce/raketasks/maintenance.html)
1. [ ] Learn about the [GitLab check](http://docs.gitlab.com/ce/raketasks/check.html) rake command
1. [ ] Learn about GitLab Omnibus commands (`gitlab-ctl`)
   1. [ ] [GitLab Status](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#get-service-status)
   1. [ ] [Starting and stopping GitLab services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/README.md#starting-and-stopping)
1. [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)

**Custom Integrations**
**Goal:** Understand how developers and customers can produce custom integrations to GitLab. If you want to integrate with GitLab there are three possible paths you can take:
1. [ ] [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
1. [ ] [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
1. [ ] [Project Services](https://docs.gitlab.com/ce/user/project/integrations/project_services.html) - Project services give you the option to build your product into GitLab itself. This is the most advanced and complex method of integration but is by far the richest. It requires you to add your integration to GitLab's code base as a standard contribution. If you're thinking of creating a project service, the steps to follow are similar to any contribution to the GitLab codebase.

**Migrations**
**Goal:** Understand how migrations from other Version Control Systems to Git & GitLab work.

1. [ ] [SVN](https://git-scm.com/book/en/v1/Git-and-Other-Systems-Git-and-Subversion)
1. [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/workflow/importing/migrating_from_svn.html)

**Learn Terraform**
1. [ ] Terraform single instance of GitLab into AWS (write your own Terraform).
   - Get AWS login from your manager
1. [ ] Terraform HA configuration into AWS (use existing Terraform).
1. [ ] Terraform HA+GEO configuration into AWS (use existing Terraform).
   - [ ] Setup cloudwatch dashboard for monitoring.
   - [ ] Create and run SAST project.
   - [ ] Setup code quality CI job (with code climate engine).
1. [ ] Terraform single instance into GCP (write your own Terraform).
   - [ ] Configure auto-devops to k8s cluster in GCP (write Terraform to create k8s cluster).
   - [ ] Create and run DAST project (use auto-devops and k8s cluster you created).
   - [ ] Setup DNS for k8s “production” cluster.
   - [ ] Setup auto-monitoring for app deployed to k8s cluster.
1. [ ] Helm deploy new cloud native containers into GCP.
1. [ ] Fix broken instance (mix of SG and misconfiguration scenarios).
   - [ ] Bad DB connection.
   - [ ] Bad Redis connection.
   - [ ] Bad NFS permissions.
   - [ ] Worker not connecting.
   - [ ] Bad certificate.
   - [ ] Can’t upload to registry.
   - [ ] Elasticsearch not indexing.
   - [ ] Repository is corrupt.

</details>

