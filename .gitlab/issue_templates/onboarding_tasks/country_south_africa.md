### For team members in South Africa

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Refer to the South Africa benefits [page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/remote-com/#south-africa) for a full overview of benefits currently offered in your region.
</details>

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.

</details>
