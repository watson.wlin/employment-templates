### Before Starting at GitLab - Team Members in Japan

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Two weeks before the team member's start date, send the forms below for them to complete, with the hire date as the effective date, and upload to `Payroll` folder in BambooHR. This will enable our payroll provider to help us with the Health Insurance registration:  
  * [ ] [Employee Registration Form_BDO](https://docs.google.com/spreadsheets/d/1JilpntlWfBpi_7q6Ofz4P2R9EDGeIo8C/edit#gid=1243702652)
        - select make a copy and save in team members name. Share with team members personal email address. 
  * [ ] [Application for (change in) exemption for dependents of employment income earner](https://drive.google.com/drive/u/0/folders/1zQ0aYTeor59dszMfYH4j7D4a3OB8Z9_Q)
        - Download and email the form to the team members personal email address to complete.
1. [ ] People Experience: CC the Non US Payroll team `nonuspayroll@gitlab.com` in the email sent to the team member so that they can see action has been taken.
</details>
