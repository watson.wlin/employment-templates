#### Backend Engineering

<details>
<summary>New Team Member</summary>

1. [ ] Join the [`#database` channel](https://gitlab.slack.com/archives/C3NBYFJ6N) on Slack. 
1. [ ] Familiarize yourself with the [`#database-lab`](https://docs.gitlab.com/ee/development/understanding_explain_plans.html#database-lab). This can be helpful to verify query plans and execute DDL statements in a clone of the production database.

</details>

<details>
<summary>Manager</summary>

1. [ ] Make sure all the (relevant) steps for Production Engineering are covered in this issue.

</details>
