### For GitLab Inc team members only

People Experience

1. [ ] People Experience: Notify payroll team @Javonna of offboarding.

Payroll @Javonna

1. [ ] Update team member status to ```Terminated``` in ADP.
1. [ ] Inactivate deductions under pay profile in ADP.

